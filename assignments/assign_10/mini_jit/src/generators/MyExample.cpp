#include "MyExample.h"

uint32_t ( *mini_jit::generators::MyExample::generate(uint32_t result) )() {
  uint32_t l_ins = 0;

    // init output
    l_ins = instructions::Base::dpMovImm(   0,
                                            0,
                                            0 );
    m_kernel.addInstruction( l_ins );


    // set values to register 1 and 2
    l_ins = instructions::Base::dpMovImm(   1,
                                            5,
                                            0 );
    m_kernel.addInstruction( l_ins );

    l_ins = instructions::Base::dpMovImm(   2,
                                            3,
                                            0 );
    m_kernel.addInstruction( l_ins );


    mini_jit::instructions::Base::shift_t shift_type = mini_jit::instructions::Base::shift_t::lsl;

    // register 0 = register 1 - register 2
    l_ins = instructions::Base::dpSubSr(    0,
                                            1,
                                            2,
                                            0,
                                            shift_type,
                                            0);
    m_kernel.addInstruction( l_ins );


    // ret
    l_ins = instructions::Base::bRet();
    m_kernel.addInstruction( l_ins );


  // we might debug through file-io
  // std::string l_file = "simple.bin";
  // m_kernel.write( l_file.c_str() );

  m_kernel.setKernel();

  return (uint32_t (*)()) m_kernel.getKernel();
}