#include "SmallGemmSve.h"
#include "../instructions/Base.h"
#include "../instructions/Sve.h"

void ( *mini_jit::generators::SmallGemmSve::generate( uint32_t i_m,
                                                      uint32_t i_n,
                                                      uint32_t i_k ) )( float const * i_a,
                                                                        float const * i_b,
                                                                        float       * io_c ) {
  uint32_t l_ins = 0;

  uint8_t pred_reg = 0;

  uint8_t i_a_reg = 0;
  uint8_t i_b_reg = 1;
  uint8_t io_c_reg = 2;

  uint8_t loop_k_reg = 3;
  uint8_t loop_m_reg = 4;
  uint8_t loop_n_reg = 5;

  uint32_t l_jumpPc;

  mini_jit::instructions::Sve::sizespec_t b = mini_jit::instructions::Sve::sizespec_t::b;
  mini_jit::instructions::Sve::sizespec_t s = mini_jit::instructions::Sve::sizespec_t::s;

  // set predicate register to true
  l_ins = instructions::Sve::prPtrue(   pred_reg,
                                        b);

  m_kernel.addInstruction( l_ins );

  // store according to procedure call standard
  m_kernel.addInstruction( 0xA9BF53F3 );
  m_kernel.addInstruction( 0xA9BF5BF5 );
  m_kernel.addInstruction( 0xA9BF63F7 );
  m_kernel.addInstruction( 0xA9BF6BF9 );
  m_kernel.addInstruction( 0xA9BF73FB );
  m_kernel.addInstruction( 0xA9BF7BFD );

  m_kernel.addInstruction( 0x6DBF27E8 );
  m_kernel.addInstruction( 0x6DBF2FEA );
  m_kernel.addInstruction( 0x6DBF37EC );
  m_kernel.addInstruction( 0x6DBF3FEE );



    // set loop counter n
    l_ins = instructions::Base::dpMovImm(   loop_n_reg,
                                            i_n,
                                            0 );
    m_kernel.addInstruction( l_ins );

    // push offset for n loop
    m_kernel.pushOffset();

    l_ins = instructions::Base::dpSubImm(   loop_n_reg,
                                            loop_n_reg,
                                            6,
                                            0 );
    m_kernel.addInstruction( l_ins );


    
    // set loop counter m
    l_ins = instructions::Base::dpMovImm(   loop_m_reg,
                                            i_m,
                                            0 );
    m_kernel.addInstruction( l_ins );

    // push offset for m loop
    m_kernel.pushOffset();

    l_ins = instructions::Base::dpSubImm(   loop_m_reg,
                                            loop_m_reg,
                                            64,
                                            0 );
    m_kernel.addInstruction( l_ins );


    // load C in registers
    for (int i = 0; i < 24; i++) {
        l_ins = instructions::Sve::lsLdrVec(    i,
                                                io_c_reg,
                                                0 );
        m_kernel.addInstruction( l_ins );


        if (i + 1 < 24) {

            uint32_t addVal = 0;
            if ((i + 1) % 4 == 0) addVal = 16*4+(i_m - 64)*4;
            else addVal = 16*4;

            l_ins = instructions::Base::dpAddImm(   io_c_reg,
                                                    io_c_reg,
                                                    addVal,
                                                    0 );
            m_kernel.addInstruction( l_ins );
        }
        else {
            l_ins = instructions::Base::dpSubImm(   io_c_reg,
                                                    io_c_reg,
                                                    5*i_m*4+3*16*4,
                                                    0 );
            m_kernel.addInstruction( l_ins );
        }
    }

    // set loop counter k
    l_ins = instructions::Base::dpMovImm(   loop_k_reg,
                                            i_k,
                                            0 );
    m_kernel.addInstruction( l_ins );

    // push offset for k loop
    m_kernel.pushOffset();

    l_ins = instructions::Base::dpSubImm(   loop_k_reg,
                                            loop_k_reg,
                                            1,
                                            0 );
    m_kernel.addInstruction( l_ins );


    //broadcast B
    for (int i = 24; i < 30; i++) {
        l_ins = instructions::Sve::lsLd1Rw( i,
                                            pred_reg,
                                            i_b_reg,
                                            0 );
        m_kernel.addInstruction( l_ins );


        if (i + 1 < 30) {
            l_ins = instructions::Base::dpAddImm(   i_b_reg,
                                                    i_b_reg,
                                                    i_k*4,
                                                    0 );
            m_kernel.addInstruction( l_ins );
        }
        else {
            l_ins = instructions::Base::dpSubImm(   i_b_reg,
                                                    i_b_reg,
                                                    5*i_k*4 - 4,
                                                    0 );
            m_kernel.addInstruction( l_ins );
        }
    }

    // load 2*16 entries of A
    for (int i = 30; i < 32; i++) {
        l_ins = instructions::Sve::lsLdrVec(    i,
                                                i_a_reg,
                                                0 );
        m_kernel.addInstruction( l_ins );

        l_ins = instructions::Base::dpAddImm(   i_a_reg,
                                                i_a_reg,
                                                16*4,
                                                0 );
        m_kernel.addInstruction( l_ins );
    }


    // perform the fmas

    for (int i = 0; i < 6; i++) {
        l_ins = instructions::Sve::dpFmlaVec(   i * 4,
                                                pred_reg,
                                                24 + i,
                                                30,
                                                s);
        m_kernel.addInstruction( l_ins );
    }

    for (int i = 0; i < 6; i++) {
        l_ins = instructions::Sve::dpFmlaVec(   1 + i * 4,
                                                pred_reg,
                                                24 + i,
                                                31,
                                                s);
        m_kernel.addInstruction( l_ins );
    }
    

    // load 2*16 entries of A
    
    l_ins = instructions::Sve::lsLdrVec(    30,
                                            i_a_reg,
                                            0 );
    m_kernel.addInstruction( l_ins );

    l_ins = instructions::Base::dpAddImm(   i_a_reg,
                                            i_a_reg,
                                            16*4,
                                            0 );
    m_kernel.addInstruction( l_ins );


    l_ins = instructions::Sve::lsLdrVec(    31,
                                            i_a_reg,
                                            0 );
    m_kernel.addInstruction( l_ins );

    l_ins = instructions::Base::dpAddImm(   i_a_reg,
                                            i_a_reg,
                                            16*4+(i_m-64)*4,
                                            0 );
    m_kernel.addInstruction( l_ins );
    


    // perform the fmas

    for (int i = 0; i < 6; i++) {
        l_ins = instructions::Sve::dpFmlaVec(   2 + i * 4,
                                                pred_reg,
                                                24 + i,
                                                30,
                                                s);
        m_kernel.addInstruction( l_ins );
    }

    for (int i = 0; i < 6; i++) {
        l_ins = instructions::Sve::dpFmlaVec(   3 + i * 4,
                                                pred_reg,
                                                24 + i,
                                                31,
                                                s);
        m_kernel.addInstruction( l_ins );
    }
  
    // loop k check
    l_jumpPc = m_kernel.popOffset() / 4 - m_kernel.getOffset() / 4;;
    l_ins = instructions::Base::bCbnz(  loop_k_reg,
                                        l_jumpPc,
                                        0 );
    m_kernel.addInstruction( l_ins );


    // store 6*64 accumulate-block of C
    for (int i = 0; i < 24; i++) {
        l_ins = instructions::Sve::lsStrVec(    i,
                                                io_c_reg,
                                                0 );
        m_kernel.addInstruction( l_ins );


        if (i + 1 < 24) {

            uint32_t addVal = 0;
            if ((i + 1) % 4 == 0) addVal = 16*4+64*4;
            else addVal = 16*4;

            l_ins = instructions::Base::dpAddImm(   io_c_reg,
                                                    io_c_reg,
                                                    addVal,
                                                    0 );
            m_kernel.addInstruction( l_ins );
        }
    }

    // prep pointers for next iteration of m
    l_ins = instructions::Base::dpSubImm(   i_a_reg,
                                            i_a_reg,
                                            8*i_m*4,
                                            0 );
    m_kernel.addInstruction( l_ins );

    l_ins = instructions::Base::dpAddImm(   i_a_reg,
                                            i_a_reg,
                                            (i_m-64)*4,
                                            0 );
    m_kernel.addInstruction( l_ins );

    l_ins = instructions::Base::dpSubImm(   i_b_reg,
                                            i_b_reg,
                                            i_n*4,
                                            0 );
    m_kernel.addInstruction( l_ins );

    l_ins = instructions::Base::dpSubImm(   io_c_reg,
                                            io_c_reg,
                                            5*i_m*4-16*4,
                                            0 );
    m_kernel.addInstruction( l_ins );


    // loop m check
    l_jumpPc = m_kernel.popOffset() / 4 - m_kernel.getOffset() / 4;;
    l_ins = instructions::Base::bCbnz(  loop_m_reg,
                                        l_jumpPc,
                                        0 );
    m_kernel.addInstruction( l_ins );


    // prep pointers for next iteration of n
    l_ins = instructions::Base::dpSubImm(   i_a_reg,
                                            i_a_reg,
                                            i_m*4,
                                            0 );
    m_kernel.addInstruction( l_ins );

    l_ins = instructions::Base::dpAddImm(   i_b_reg,
                                            i_b_reg,
                                            6*i_k*4,
                                            0 );
    m_kernel.addInstruction( l_ins );

    l_ins = instructions::Base::dpAddImm(   io_c_reg,
                                            io_c_reg,
                                            5*i_m*4,
                                            0 );
    m_kernel.addInstruction( l_ins );


    // loop n check
    l_jumpPc = m_kernel.popOffset() / 4 - m_kernel.getOffset() / 4;
    l_ins = instructions::Base::bCbnz(  loop_n_reg,
                                        l_jumpPc,
                                        0 );
    m_kernel.addInstruction( l_ins );


  // restore according to procedure call standard
  m_kernel.addInstruction( 0x6CC13FEE );
  m_kernel.addInstruction( 0x6CC137EC );
  m_kernel.addInstruction( 0x6CC12FEA );
  m_kernel.addInstruction( 0x6CC127E8 );

  m_kernel.addInstruction( 0xA8C17BFD );
  m_kernel.addInstruction( 0xA8C173FB );
  m_kernel.addInstruction( 0xA8C16BF9 );
  m_kernel.addInstruction( 0xA8C163F7 );
  m_kernel.addInstruction( 0xA8C15BF5 );
  m_kernel.addInstruction( 0xA8C153F3 );

  // ret
  l_ins = instructions::Base::bRet();
  m_kernel.addInstruction( l_ins );

  // we might debug through file-io
  std::string l_file = "small_gemm_sve.bin";
  m_kernel.write( l_file.c_str() );

  m_kernel.setKernel();

  return (void (*)( float const *,
                    float const *,
                    float       *  )) m_kernel.getKernel();
}
