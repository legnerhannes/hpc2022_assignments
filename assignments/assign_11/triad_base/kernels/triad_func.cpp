#include "triad.h"

void triad_single(float const * __restrict i_a, float const * __restrict i_b, float * __restrict o_c);

void triad_func( uint64_t         i_nValues,
                   float    const * __restrict i_a,
                   float    const * __restrict i_b,
                   float          * __restrict o_c ) {
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
    triad_single(i_a + l_va, i_b + l_va, o_c + l_va);
  }
}