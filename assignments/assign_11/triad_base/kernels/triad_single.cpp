void triad_single(float const * __restrict i_a, float const * __restrict i_b, float * __restrict o_c) {
    *o_c = *i_a + 2.0f * *i_b;
}