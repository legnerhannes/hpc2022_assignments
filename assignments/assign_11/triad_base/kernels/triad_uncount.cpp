#include "triad.h"

void triad_uncount( uint64_t         i_nValues,
                   float    const * __restrict i_a,
                   float    const * __restrict i_b,
                   float          * __restrict o_c ) {
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {

    if (i_a[l_va] < 0 && i_b[l_va] > 0 ) break;

    o_c[l_va] = i_a[l_va] + 2.0f * i_b[l_va];
  }
}