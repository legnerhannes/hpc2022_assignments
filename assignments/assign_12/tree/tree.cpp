#include <iostream>
#include <chrono>
#include <random>
#include <cmath>

class Node {
  private:
    //! data item of the node
    double m_data = 0.0;
    //! pointer to left child if any
    Node * m_left = nullptr;
    //! pointer to right child if any
    Node * m_right = nullptr;

  public:
    /**
     * Destructor of the node.
     * Frees all allocated memory.
     **/
    ~Node() {
      if( m_left  != nullptr ) delete m_left;
      if( m_right != nullptr ) delete m_right;
    }

    // TODO: finish implementation

    void SetM_left(Node * left);
    void SetM_right(Node * right);
};

void Node::SetM_left(Node * left) {
    std::cout << "l1 node" << std::endl;
    m_left = left;
    std::cout << "l2 node" << std::endl;
}

void Node::SetM_right(Node * right) {
    std::cout << "r1 node" << std::endl;
    m_right = right;
    std::cout << "r2 node" << std::endl;
}



class Tree {
  private:
    //! root node of the tree
    Node * m_root = nullptr;

  public:

    Tree(double rLeft, double rRight) {

        std::random_device r_dev;
        std::mt19937 r_mt (r_dev());

        std::cout << "root node" << std::endl;

        m_root = initRandom(0, rLeft, rRight, r_mt);
        

    }
    /**
     * Destructor which frees all allocated memory.
     **/
    ~Tree() {
      if( m_root != nullptr ) delete m_root;
    };

    /**
     * Randomly initializes the tree.
     *
     * @param i_n_levels number of levels in the tree.
     * @param i_probLeft probability that a node has a left child.
     * @param i_probRight probability that a node has right child.
     * @param i_generator random number generator.
     **/
    Node* initRandom(    uint64_t       i_nLevels,
                        double         i_probLeft,
                        double         i_probRight,
                        std::mt19937 & i_generator );

    /**
     * Applies a function to all data items of the tree's nodes.
     *
     * @param i_func function which is applied.
     *               has the current level, parent node's data and node's data as args.
     **/
    void applyFunction( double(* i_func)( uint64_t i_lvl,
                                          double   i_dataParent,
                                          double   i_dataNode ) );
};

void Tree::applyFunction(double(* i_func)(  uint64_t i_lvl,
                                            double   i_dataParent,
                                            double   i_dataNode )) {
        
}

// TODO: finish implementation
Node* Tree::initRandom(  uint64_t i_nLevels,
                        double i_probLeft,
                        double i_probRight,
                        std::mt19937 &i_generator)
{   

    std::uniform_real_distribution<double> dist(0.0, 1.0);

    Node* node;

    if (dist(i_generator) < i_probLeft) {
        std::cout << "left node lvl " << i_nLevels + 1 << std::endl;
        Node leftNode = *node;
        leftNode.SetM_left(initRandom(i_nLevels + 1, i_probLeft, i_probRight, i_generator));
        *node = leftNode;
        std::cout << "cont left node lvl " << i_nLevels + 1 << std::endl;
    }

    std::cout << "between" << std::endl;
    

    if (dist(i_generator) > i_probRight) {
        std::cout << "right node lvl " << i_nLevels + 1 << std::endl;
        Node rightNode = *node;
        rightNode.SetM_right(initRandom(i_nLevels + 1, i_probLeft, i_probRight, i_generator));
        *node = rightNode;
        std::cout << "cont right node lvl " << i_nLevels + 1 << std::endl;
    }

    return node;
}

int main() {
    Tree tree = Tree(0.75, 0.75);
}