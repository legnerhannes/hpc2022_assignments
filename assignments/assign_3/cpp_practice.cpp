#include <cstdlib>
#include <iostream>

int main() {
    std::cout << "unsigned char:\t" << sizeof(unsigned char) << std::endl;
    std::cout << "char:\t" << sizeof(char) << std::endl;
    std::cout << "unsigned int:\t" << sizeof(unsigned int) << std::endl;
    std::cout << "float:\t" << sizeof(float) << std::endl;
    std::cout << "double:\t" << sizeof(double) << std::endl;
    std::cout << "unsigned int *:\t" << sizeof(unsigned int *) << std::endl;
    std::cout << "float *:\t" << sizeof(float *) << std::endl;

    int * l_my_var = (int *) malloc(1500 * sizeof(int));

    for (int i = 0; i < 1500; i++) {
        l_my_var[i] = 3 * i;
    }

    std::cout << "value at 500:\t" <<  *(l_my_var + 500) << std::endl;
    std::cout << "adress at 250:\t" <<  l_my_var + 250 << std::endl;
    std::cout << "adress at 750:\t" <<  l_my_var + 750 << std::endl;
    std::cout << "distance:\t" <<  (l_my_var + 750) - (l_my_var + 250) << std::endl;

    free(l_my_var);

    return EXIT_SUCCESS;
}