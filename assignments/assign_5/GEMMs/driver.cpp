#include <cstdint>
#include <cmath>
#include <chrono>
#include <omp.h>
#include <iostream>
#include "kernels/gemm_ref.h"
#include "kernels/gemm_ref.cpp"
#include "kernels/gemm_compiler_32_32_32_32_32_32.cpp"

const uint64_t l_n_repetitions = 100000;

void unit_test(unsigned int m, unsigned int n, unsigned int k, unsigned int lda, unsigned int ldb, unsigned int ldc) {

    float * l_a = new float[lda * k];
    float * l_b = new float[ldb * n];
    float * l_c = new float[ldc * n];


    // values in matrix = 2, values in ld range = 0 
    // set and print A
    std::cout << "\nA:"<< std::endl;
    for (int i = 0; i < k; i ++)
    {
        for (int j = 0; j < lda; j++) {
            if (j >= m) l_a[j + lda * i] = 0;
            else l_a[j + lda * i] = 2;
        }
    }

    for (int i = 0; i < lda; i ++)
    {   
        for (int j = 0; j < k; j++) {
            std::cout << l_a[lda * j + i] << "\t";
        }
        std::cout << std::endl;
    }

    // set and print B
    std::cout << "\nB:"<< std::endl;

    for (int i = 0; i < n; i ++)
    {
        for (int j = 0; j < ldb; j++) {
            if (j >= k) l_b[j + ldb * i] = 0;
            else l_b[j + ldb * i] = 2;
        }
    }

    for (int i = 0; i < ldb; i ++)
    {   
        for (int j = 0; j < n; j++) {
            std::cout <<  l_b[ldb * j + i] << "\t";
        }
        std::cout << std::endl;
    }


    //call kernel
    gemm_ref(l_a, l_b, l_c, m, n, k, lda, ldb, ldc);


    // print C
    std::cout << "\nC:"<< std::endl;

    for (int i = 0; i < ldc; i ++)
    {   
        for (int j = 0; j < n; j++) {
            std::cout << l_c[ldc * j + i] << "\t";
        }
        std::cout << std::endl;
    }

    delete[] l_a;
    delete[] l_b;
    delete[] l_c;
}



int main() {
    std::cout << "Start GEMM test kernel" << std::endl;

    double gflops = 0;
    int m_size = 32;
    int threads = 12;


    std::chrono::high_resolution_clock::time_point l_tp0, l_tp1;
    std::chrono::duration< double > l_dur;

    l_tp0 = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < l_n_repetitions; i++) {
        //gflops += gemm_ref(new float[m_size * m_size], new float[m_size * m_size], new float[m_size * m_size],m_size,m_size,m_size,m_size,m_size,m_size);
        gflops += gemm_compiler_32_32_32_32_32_32_mnk(new float[m_size * m_size], new float[m_size * m_size], new float[m_size * m_size]);
    }

    l_tp1 = std::chrono::high_resolution_clock::now();
    l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );

    std::cout << "Loop duration: " << l_dur.count() << std::endl;

    //gflops *= l_n_repetitions;
    gflops *= threads;
    gflops *= 1.0E-9;
    gflops /= l_dur.count();
    std::cout << "GFLOPS: " << gflops << std::endl;


    /*
    Unit test Calls

    unit_test(3,3,3,3,3,3);
    unit_test(3,3,3,4,5,6);
    unit_test(3,4,5,5,5,5);
    unit_test(2,8,5,2,7,4);
    unit_test(32, 8, 16, 40, 24, 32);

    printed results:
    A:
    2       2       2
    2       2       2
    2       2       2

    B:
    2       2       2
    2       2       2
    2       2       2

    C:
    12      12      12
    12      12      12
    12      12      12

    A:
    2       2       2
    2       2       2
    2       2       2
    0       0       0

    B:
    2       2       2
    2       2       2
    2       2       2
    0       0       0
    0       0       0

    C:
    12      12      12
    12      12      12
    12      12      12
    0       0       0
    0       0       0
    0       0       0

    A:
    2       2       2       2       2
    2       2       2       2       2
    2       2       2       2       2
    0       0       0       0       0
    0       0       0       0       0

    B:
    2       2       2       2
    2       2       2       2
    2       2       2       2
    2       2       2       2
    2       2       2       2

    C:
    20      20      20      20
    20      20      20      20
    20      20      20      20
    0       0       0       0
    0       0       0       0

    A:
    2       2       2       2       2
    2       2       2       2       2

    B:
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0

    C:
    20      20      20      20      20      20      20      20
    20      20      20      20      20      20      20      20
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0

    A:
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2       2       2       2       2       2       2       2       2
    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0

    B:
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    2       2       2       2       2       2       2       2
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0
    0       0       0       0       0       0       0       0

    C:
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    64      64      64      64      64      64      64      64
    */
}