int gemm_compiler_32_32_32_32_32_32_mnk( float const * i_a,
                                          float const * i_b,
                                          float       * io_c ) {

    const int size = 32;
    int instr_count = 0;

    for (unsigned int l_m = 0; l_m < size; l_m++) {
        for (unsigned int l_n = 0; l_n < size; l_n++) {
            for (unsigned int l_k = 0; l_k < size; l_k++) {
                io_c[l_n * size + l_m] += i_a[l_k * size + l_m] * i_b[l_n * size + l_k];
                instr_count += 2;
            }
        }
    }

    return instr_count;
}

int gemm_compiler_32_32_32_32_32_32_nkm( float const * i_a,
                                          float const * i_b,
                                          float       * io_c ) {

    const int size = 32;
    int instr_count = 0;

    for (unsigned int l_n = 0; l_n < size; l_n++) {
        for (unsigned int l_k = 0; l_k < size; l_k++) {
            for (unsigned int l_m = 0; l_m < size; l_m++) {
                io_c[l_n * size + l_m] += i_a[l_k * size + l_m] * i_b[l_n * size + l_k];
                instr_count += 2;
            }
        }
    }

    return instr_count;
}