int gemm_ref( float const * i_a,
               float const * i_b,
               float * io_c,
               unsigned int i_m,
               unsigned int i_n,
               unsigned int i_k,
               unsigned int i_lda,
               unsigned int i_ldb,
               unsigned int i_ldc );

int gemm_compiler_32_32_32_32_32_32_mnk( float const * i_a,
                                          float const * i_b,
                                          float       * io_c );


int gemm_compiler_32_32_32_32_32_32_nkm( float const * i_a,
                                          float const * i_b,
                                          float       * io_c );