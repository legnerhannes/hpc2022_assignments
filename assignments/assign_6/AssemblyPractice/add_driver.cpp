#include <bitset>
#include <iostream>

extern "C" {
    uint64_t data_processing_1( uint64_t const * i_a,
                                uint64_t const * i_b,
                                uint64_t       * i_c );
}

int main() {

    uint64_t l_a1 = 3;
    uint64_t l_b1 = 4;
    uint64_t l_c1 = 0;
    uint64_t l_nzcv1 = data_processing_1( &l_a1,
                                  &l_b1,
                                  &l_c1 );

    std::cout << "data_processing_1("
              << l_a1  << ", "
              << l_b1 << ") #1: "
              << l_c1 << " / "
              << std::bitset< 64 >( l_nzcv1 )
              << std::endl;

    l_a1 = 0xffffffffffffffff;
    l_b1 = 5;
    l_c1 = 0;
    l_nzcv1 = data_processing_1( &l_a1,
                         &l_b1,
                         &l_c1 );

    std::cout << "data_processing_1("
              << l_a1  << ", "
              << l_b1 << ") #2: "
              << l_c1 << " / "
              << std::bitset< 64 >( l_nzcv1 )
              << std::endl;

    return EXIT_SUCCESS;
}