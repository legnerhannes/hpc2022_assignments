.text
.type data_processing_1, %function
.global data_processing_1
/*
 * Adds two 64-bit values.
 *
 * @param x0: address of first 64-bit value.
 * @param x1: address of second 64-bit value.
 * @param x2: address for the 64-bit result.
 * @return x0: NZCV after the add-operation.
 */

data_processing_1:
    ldr x3, [x0]
    ldr x4, [x1]
    adds x3, x3, x4
    mrs x0, nzcv
    str x3, [x2]

    ret
    .size data_processing_1, (. - data_processing_1)