        .text
        .align 4
        .type   copy_asm, %function
        .global copy_asm

copy_asm:

        # load values from array l_a in register
        ldr x10, [x0,#4*0]
        ldr x11, [x0,#4*1]
        ldr x12, [x0,#4*2]
        ldr x13, [x0,#4*3]
        ldr x14, [x0,#4*4]
        ldr x15, [x0,#4*5]
        ldr x16, [x0,#4*6]

        # Masking register because because l_b has 8 byte and the first 4 bytes of the next value would leak into the current value
        and x10, x10, 0x0000FFFF
        and x11, x11, 0x0000FFFF
        and x12, x12, 0x0000FFFF
        and x13, x13, 0x0000FFFF
        and x14, x14, 0x0000FFFF
        and x15, x15, 0x0000FFFF
        and x16, x16, 0x0000FFFF

        # store masked values from register in array l_b
        str x10, [x1,#8*0]
        str x11, [x1,#8*1]
        str x12, [x1,#8*2]
        str x13, [x1,#8*3]
        str x14, [x1,#8*4]
        str x15, [x1,#8*5]
        str x16, [x1,#8*6]


        ret
        .size copy_asm, (. - copy_asm)