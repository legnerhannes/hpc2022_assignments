        .text
        .type gemm_asm_gp, %function
        .global gemm_asm_gp
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (4x2) = (4x2) * (2x2).
         * The input-data is of type uint32_t.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */ 

gemm_asm_gp:
        // store
        stp x19, x20, [sp, #-16]!
        stp x21, x22, [sp, #-16]!
        stp x23, x24, [sp, #-16]!
        stp x25, x26, [sp, #-16]!
        stp x27, x28, [sp, #-16]!
        stp x29, x30, [sp, #-16]!

        # load C
        ldp w3, w4, [x2, #0*4]
        ldp w5, w6, [x2, #2*4]
        ldp w7, w8, [x2, #4*4]
        ldp w9, w10, [x2, #6*4]

        # load first column of A
        ldp w11, w12, [x0]
        ldp w13, w14, [x0, #2*4]

        # load first entry of B
        ldr w15, [x1]

        madd w3, w11, w15, w3
        madd w4, w12, w15, w4
        madd w5, w13, w15, w5
        madd w6, w14, w15, w6

        # load third entry of B
        ldr w15, [x1, #2*4]

        madd w7, w11, w15, w7
        madd w8, w12, w15, w8
        madd w9, w13, w15, w9
        madd w10, w14, w15, w10

        # load second column of A
        ldp w11, w12, [x0, #4*4]
        ldp w13, w14, [x0, #6*4]

        # load second entry of B
        ldr w15, [x1, #1*4]

        madd w3, w11, w15, w3
        madd w4, w12, w15, w4
        madd w5, w13, w15, w5
        madd w6, w14, w15, w6

        # load forth entry of B
        ldr w15, [x1, #3*4]

        madd w7, w11, w15, w7
        madd w8, w12, w15, w8
        madd w9, w13, w15, w9
        madd w10, w14, w15, w10

        # store C
        stp w3, w4, [x2, #0*4]
        stp w5, w6, [x2, #2*4]
        stp w7, w8, [x2, #4*4]
        stp w9, w10, [x2, #6*4]

        // restore
        ldp x29, x30, [sp], #16
        ldp x27, x28, [sp], #16
        ldp x25, x26, [sp], #16
        ldp x23, x24, [sp], #16
        ldp x21, x22, [sp], #16
        ldp x19, x20, [sp], #16

        ret
        .size gemm_asm_gp, (. - gemm_asm_gp)