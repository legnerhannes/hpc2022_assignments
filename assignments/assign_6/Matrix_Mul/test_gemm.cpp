#include <cstdint>
#include <cstdlib>
#include <iostream>

extern "C" {
    void gemm_asm_gp(   uint32_t const * i_a,
                        uint32_t const * i_b,
                        uint32_t       * io_c );
}

void printMatrix(uint32_t *m, int x, int y) {
    std::cout << "matrix:" << std::endl;
    for (int i = 0; i < y; i++) {
        for (int j = 0; j < x; j++) {
            std::cout << m[i +  y*j] << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


int main() {
    uint32_t *A = (uint32_t *) malloc(8 * sizeof(uint32_t));

    A[0] = 8;
    A[1] = 6;
    A[2] = 9;
    A[3] = 2;
    A[4] = 3;
    A[5] = 5;
    A[6] = 5;
    A[7] = 7;

    printMatrix(A, 2, 4);


    uint32_t *B = (uint32_t *) malloc(4 * sizeof(uint32_t));

    B[0] = 4;
    B[1] = 9;
    B[2] = 5;
    B[3] = 1;

    printMatrix(B, 2, 2);


    uint32_t *C = (uint32_t *) malloc(8 * sizeof(uint32_t));

    gemm_asm_gp(A, B, C);

    printMatrix(C, 2, 4);
    

    free(A);
    free(B);
    free(C);
}