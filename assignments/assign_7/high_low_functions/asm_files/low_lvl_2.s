        .text
        .align 4
        .type   low_lvl_2, %function
        .global low_lvl_2
        
low_lvl_2:
        mov x1, #32
        cmp x0, x1
        mrs x0, nzcv

        ret 
        .size low_lvl_2, (. - low_lvl_2)