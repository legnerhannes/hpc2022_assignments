        .text
        .align 4
        .type   low_lvl_5, %function
        .global low_lvl_5
        
low_lvl_5:
        ldr x2, [x1]
        mov x3, x0

        loop:
        sub x3, x3, #1
        add x2, x2, #1
        cbnz x3, loop

        str x2, [x1]

        ret
        .size low_lvl_5, (. - low_lvl_5)