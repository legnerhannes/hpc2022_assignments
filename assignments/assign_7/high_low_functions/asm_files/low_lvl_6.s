        .text
        .align 4
        .type   low_lvl_6, %function
        .global low_lvl_6
        
low_lvl_6:
        ldr x3, [x2]
        mov x4, x0

        loop:
        add x3, x3, x1
        sub x4, x4, #1
        cbnz x4, loop

        str x3, [x2]

        ret
        .size low_lvl_6, (. - low_lvl_6)