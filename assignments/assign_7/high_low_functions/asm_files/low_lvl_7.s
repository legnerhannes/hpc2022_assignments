        .text
        .align 4
        .type   low_lvl_7, %function
        .global low_lvl_7
        
low_lvl_7:
        mov x3, x0

        loop:
        ldr x4, [x1]
        ldr x5, [x2]
        
        mov x5, x4
        str x5, [x2]

        add x1, x1, #8
        add x2, x2, #8
        sub x3, x3, #1
        cbnz x3, loop

        ret
        .size low_lvl_7, (. - low_lvl_7)