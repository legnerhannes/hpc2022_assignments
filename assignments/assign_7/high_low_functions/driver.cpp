#include <iostream>
#include <cstdint>
#include "low_level.h"

int main() {
  std::cout << "running driver" << std::endl;

  std::cout << "low_lvl_0(10): "
            << low_lvl_00( 10 )
            << std::endl;
  std::cout << "low_lvl_1(10): "
            << low_lvl_01( 10 ) << std::endl;
  std::cout << "low_lvl_2(32): "
            << low_lvl_02( 32 ) << std::endl;
  std::cout << "low_lvl_2( 5): "
            << low_lvl_02(  5 ) << std::endl;

  int32_t l_lowLvlOpt3 = 17;
  int32_t l_lowLvlRes3 = -1;
  low_lvl_03( &l_lowLvlOpt3,
              &l_lowLvlRes3 );
  std::cout << "low_lvl_3 #1: "
            << l_lowLvlRes3 << std::endl;

  l_lowLvlOpt3 = 43;
  low_lvl_03( &l_lowLvlOpt3,
              &l_lowLvlRes3 );
  std::cout << "low_lvl_3 #2: "
            << l_lowLvlRes3 << std::endl;
  std::cout << "low_lvl_4(1,2,3): "
            << low_lvl_04( 1, 2, 3 ) << std::endl;
  std::cout << "low_lvl_4(4,2,3): "
            << low_lvl_04( 4, 2, 3 ) << std::endl;
  std::cout << "low_lvl_4(4,3,3): "
            << low_lvl_04( 4, 3, 3 ) << std::endl;

  int32_t l_lowLvlValue5 = 500;
  low_lvl_05(  17,
              &l_lowLvlValue5 );
  std::cout << "low_lvl_5: " << l_lowLvlValue5 << std::endl;

  int64_t l_lowLvlValue6 = 23;
  low_lvl_06( 5,
              13,
              &l_lowLvlValue6 );
  std::cout << "low_lvl_6: "
            << l_lowLvlValue6 << std::endl;

  int64_t l_lowLvlVasIn7[10] = { 0, 7, 7, 4, 3,\
                                 -10, -50, 40, 2, 3 };
  int64_t l_lowLvlVasOut7[10] = { 0 };
  low_lvl_07( 10,
              l_lowLvlVasIn7,
              l_lowLvlVasOut7 );

  std::cout << "low_lvl_7: "
            << l_lowLvlVasOut7[0] << " / "
            << l_lowLvlVasOut7[1] << " / "
            << l_lowLvlVasOut7[2] << " / "
            << l_lowLvlVasOut7[3] << " / "
            << l_lowLvlVasOut7[4] << " / "
            << l_lowLvlVasOut7[5] << " / "
            << l_lowLvlVasOut7[6] << " / "
            << l_lowLvlVasOut7[7] << " / "
            << l_lowLvlVasOut7[8] << " / "
            << l_lowLvlVasOut7[9] << std::endl;

  // low-level part goes here

  std::cout << "finished, exiting" << std::endl;
  return EXIT_SUCCESS;
}