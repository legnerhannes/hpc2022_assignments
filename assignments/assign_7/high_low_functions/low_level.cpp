#include <cstdint>
#include <iostream>

extern "C" {
    int32_t low_lvl_0(int32_t i_value);
    uint64_t low_lvl_1(uint64_t);
    int32_t low_lvl_2(int32_t i_option);
    void low_lvl_3(int32_t * i_option, int32_t * o_result);
    uint32_t low_lvl_4(uint32_t i_x, uint32_t i_y, uint32_t i_z);
    void low_lvl_5(uint32_t  i_nIters, int32_t  * io_value);
    void low_lvl_6(uint64_t  i_nIters, int64_t i_inc, int64_t * io_value);
    void low_lvl_7(uint64_t  i_nValues, int64_t * i_valuesIn, int64_t * i_valuesOut);
}

int32_t low_lvl_00( int32_t i_value ) {
    return low_lvl_0(i_value);
}

uint64_t low_lvl_01( uint64_t ) {
    return low_lvl_1(0);
}

int32_t low_lvl_02( int32_t i_option ) {
    return low_lvl_2(i_option);
}

void low_lvl_03( int32_t * i_option,
                 int32_t * o_result ) {
    low_lvl_3(i_option, o_result);
}

uint32_t low_lvl_04( uint32_t i_x,
                     uint32_t i_y,
                     uint32_t i_z ) {
    return low_lvl_4(i_x, i_y, i_z);
}

void low_lvl_05( uint32_t   i_nIters,
                 int32_t  * io_value ) {
    low_lvl_5(i_nIters, io_value);
}

void low_lvl_06( uint64_t   i_nIters,
                 int64_t    i_inc,
                 int64_t  * io_value ) {
    low_lvl_6(i_nIters, i_inc, io_value);
}

void low_lvl_07( uint64_t   i_nValues,
                 int64_t  * i_valuesIn,
                 int64_t  * i_valuesOut ) {
    low_lvl_7(i_nValues, i_valuesIn, i_valuesOut);
}