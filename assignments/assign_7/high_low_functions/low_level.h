#include <cstdint>

int32_t low_lvl_00( int32_t i_value );

uint64_t low_lvl_01( uint64_t );

int32_t low_lvl_02( int32_t i_option );

void low_lvl_03( int32_t * i_option,
                 int32_t * o_result );

uint32_t low_lvl_04( uint32_t i_x,
                     uint32_t i_y,
                     uint32_t i_z );

void low_lvl_05( uint32_t   i_nIters,
                 int32_t  * io_value );

void low_lvl_06( uint64_t   i_nIters,
                 int64_t    i_inc,
                 int64_t  * io_value );

void low_lvl_07( uint64_t   i_nValues,
                 int64_t  * i_valuesIn,
                 int64_t  * i_valuesOut );