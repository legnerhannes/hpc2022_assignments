void gemm_ref_mnk( float const * i_a,
               float const * i_b,
               float * io_c,
               unsigned int i_m,
               unsigned int i_n,
               unsigned int i_k,
               unsigned int i_lda,
               unsigned int i_ldb,
               unsigned int i_ldc );

/*void gemm_asm_sve_64_6_1(   float const * i_a,
                            float const * i_b,
                            float       * io_c );


void gemm_asm_sve_64_6_48(  float const * i_a,
                            float const * i_b,
                            float       * io_c );*/