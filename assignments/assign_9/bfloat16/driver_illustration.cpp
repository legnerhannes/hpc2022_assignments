#include <iostream>
#include <arm_bf16.h>
#include <arm_neon.h>

extern "C" {
    void gemm_asm_bf16_4_2_8( bfloat16_t const * i_a,  bfloat16_t const * i_b, float * io_c);
}

void convert_a_to_bfmmla(uint64_t m, uint64_t k, bfloat16_t const * i_a_col_major, bfloat16_t * o_a_fmmla) {
    for (int i_k = 0; i_k < k; i_k += 4) {
        for (int i_m = 0; i_m < m ; i_m += 2) {
            uint64_t i = i_k * m + i_m;

            o_a_fmmla[i + 0 * m + 0] = i_a_col_major[i * 4 + 0];
            o_a_fmmla[i + 1 * m + 0] = i_a_col_major[i * 4 + 1];
            o_a_fmmla[i + 2 * m + 0] = i_a_col_major[i * 4 + 2];
            o_a_fmmla[i + 3 * m + 0] = i_a_col_major[i * 4 + 3];
            o_a_fmmla[i + 0 * m + 1] = i_a_col_major[i * 4 + 4];
            o_a_fmmla[i + 1 * m + 1] = i_a_col_major[i * 4 + 5];
            o_a_fmmla[i + 2 * m + 1] = i_a_col_major[i * 4 + 6];
            o_a_fmmla[i + 3 * m + 1] = i_a_col_major[i * 4 + 7];
        }
    }
}


void convert_b_to_bfmmla(uint64_t k, uint64_t n, bfloat16_t const * i_a_col_major, bfloat16_t * o_a_fmmla) {
    for (int i_n = 0; i_n < n; i_n += 2) {
        for (int i_k = 0; i_k < k ; i_k += 4) {
            uint64_t i = i_n * k + i_k;

            o_a_fmmla[i + 0 * k + 0] = i_a_col_major[i + 0];
            o_a_fmmla[i + 0 * k + 1] = i_a_col_major[i + 1];
            o_a_fmmla[i + 0 * k + 2] = i_a_col_major[i + 2];
            o_a_fmmla[i + 0 * k + 3] = i_a_col_major[i + 3];
            o_a_fmmla[i + 1 * k + 0] = i_a_col_major[i + 4];
            o_a_fmmla[i + 1 * k + 1] = i_a_col_major[i + 5];
            o_a_fmmla[i + 1 * k + 2] = i_a_col_major[i + 6];
            o_a_fmmla[i + 1 * k + 3] = i_a_col_major[i + 7];
        }
    }
}


void convert_c_to_bfmmla(uint64_t m, uint64_t n, float const * i_c_col_major, float * o_c_fmmla) {
    for (int i_n = 0; i_n < n; i_n += 2) {
        for (int i_m = 0; i_m < m ; i_m += 2) {
            uint64_t i = i_n * m + i_m;

            o_c_fmmla[i + 0 * m + 0] = i_c_col_major[i_n + i_m * 2 + 0];
            o_c_fmmla[i + 0 * m + 1] = i_c_col_major[i_n + i_m * 2 + 1];
            o_c_fmmla[i + 1 * m + 0] = i_c_col_major[i_n + i_m * 2 + 2];
            o_c_fmmla[i + 1 * m + 1] = i_c_col_major[i_n + i_m * 2 + 3];
        }
    }
}


void convert_c_from_bfmmla(uint64_t m, uint64_t n, float const * o_c_fmmla, float * i_c_col_major) {
    for (int i_n = 0; i_n < n; i_n += 2) {
        for (int i_m = 0; i_m < m ; i_m += 2) {
            uint64_t i = i_n * m + i_m;

            i_c_col_major[i_n + i_m * 2 + 0] = o_c_fmmla[i + 0 * m + 0];
            i_c_col_major[i_n + i_m * 2 + 1] = o_c_fmmla[i + 0 * m + 1];
            i_c_col_major[i_n + i_m * 2 + 2] = o_c_fmmla[i + 1 * m + 0];
            i_c_col_major[i_n + i_m * 2 + 3] = o_c_fmmla[i + 1 * m + 1];
        }
    }
}



void printBfloatMatrix(bfloat16_t *m, int x, int y) {
    std::cout << "matrix:" << std::endl;
    for (int i = 0; i < y; i++) {
        for (int j = 0; j < x; j++) {
            std::cout << vcvtah_f32_bf16(m[i +  y*j]) << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void printFloatMatrix(float *m, int x, int y) {
    std::cout << "matrix:" << std::endl;
    for (int i = 0; i < y; i++) {
        for (int j = 0; j < x; j++) {
            std::cout << m[i +  y*j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}



int main() {

    bfloat16_t * l_a_bf16 = new bfloat16_t[16*4];
    bfloat16_t * l_a_bf16_bfmmla = new bfloat16_t[16*4];
    bfloat16_t * l_b_bf16 = new bfloat16_t[4*12];
    bfloat16_t * l_b_bf16_bfmmla = new bfloat16_t[4*12];
    float * l_c_fp32 = new float[16*12];
    float * l_c_fp32_bfmmla = new float[16*12];


    srand48(time(NULL));

    for(unsigned int l_id = 0; l_id < 16*4; l_id++) {
        l_a_bf16[l_id] = (bfloat16_t) drand48();
    }
    for(unsigned int l_id = 0; l_id < 4*12; l_id++) {
        l_b_bf16[l_id] = (bfloat16_t) drand48();
    }
    for(unsigned int l_id = 0; l_id < 16*12; l_id++) {
        l_c_fp32[l_id] = (float) drand48();
    }


    convert_a_to_bfmmla(16, 4, l_a_bf16, l_a_bf16_bfmmla);
    convert_b_to_bfmmla(4, 12, l_b_bf16, l_b_bf16_bfmmla);
    convert_c_to_bfmmla(16, 12, l_c_fp32, l_c_fp32_bfmmla);

    printBfloatMatrix(l_a_bf16, 4, 16);
    printBfloatMatrix(l_a_bf16_bfmmla, 4, 16);

    printBfloatMatrix(l_b_bf16, 12, 4);
    printBfloatMatrix(l_b_bf16_bfmmla, 12, 4);

    printFloatMatrix(l_c_fp32, 12, 16);
    printFloatMatrix(l_c_fp32_bfmmla, 12, 16);

    
    
    bfloat16_t _l_a_bf16[16] = { 0.0, 1.0, 2.0, 3.0,
                                4.0, 5.0, 6.0, 7.0,
                                8.0, 9.0, 10.0, 11.0,
                                12.0, 13.0, 14.0, 15.0 };
    bfloat16_t _l_b_bf16[16] = { 0.0, 1.0, 2.0, 3.0,
                                4.0, 5.0, 6.0, 7.0,
                                8.0, 9.0, 10.0, 11.0,
                                12.0, 13.0, 14.0, 15.0 };
    float _l_c_fp32[8] = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0 };

    gemm_asm_bf16_4_2_8(_l_a_bf16, _l_b_bf16, _l_c_fp32);

    std::cout << "A:" << std::endl;
    for (int i = 0; i < 16; i++) {
        if (i == 3 || i == 7 || i == 11|| i == 15) std::cout << vcvtah_f32_bf16(_l_a_bf16[i]) << std::endl;
        else std::cout << vcvtah_f32_bf16(_l_a_bf16[i]) << "\t";
    }


    std::cout << "B:" << std::endl;
    std::cout << vcvtah_f32_bf16(_l_b_bf16[0]) << "\t";
    std::cout << vcvtah_f32_bf16(_l_b_bf16[4]) << std::endl;
    std::cout << vcvtah_f32_bf16(_l_b_bf16[1]) << "\t";
    std::cout << vcvtah_f32_bf16(_l_b_bf16[5]) << std::endl;
    std::cout << vcvtah_f32_bf16(_l_b_bf16[2]) << "\t";
    std::cout << vcvtah_f32_bf16(_l_b_bf16[6]) << std::endl;
    std::cout << vcvtah_f32_bf16(_l_b_bf16[3]) << "\t";
    std::cout << vcvtah_f32_bf16(_l_b_bf16[7]) << std::endl;
    std::cout << vcvtah_f32_bf16(_l_b_bf16[8]) << "\t";
    std::cout << vcvtah_f32_bf16(_l_b_bf16[12]) << std::endl;
    std::cout << vcvtah_f32_bf16(_l_b_bf16[9]) << "\t";
    std::cout << vcvtah_f32_bf16(_l_b_bf16[13]) << std::endl;
    std::cout << vcvtah_f32_bf16(_l_b_bf16[10]) << "\t";
    std::cout << vcvtah_f32_bf16(_l_b_bf16[14]) << std::endl;
    std::cout << vcvtah_f32_bf16(_l_b_bf16[11]) << "\t";
    std::cout << vcvtah_f32_bf16(_l_b_bf16[15]) << std::endl;

    std::cout << "C:" << std::endl;
    std::cout << _l_c_fp32[0] << "\t";
    std::cout << _l_c_fp32[2] << std::endl;
    std::cout << _l_c_fp32[1] << "\t";
    std::cout << _l_c_fp32[3] << std::endl;
    std::cout << _l_c_fp32[4] << "\t";
    std::cout << _l_c_fp32[6] << std::endl;
    std::cout << _l_c_fp32[5] << "\t";
    std::cout << _l_c_fp32[7] << std::endl;
    
    return EXIT_SUCCESS;
}