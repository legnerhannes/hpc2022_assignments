#include <iostream>
#include <cmath>
#include <chrono>

extern "C" {
    void triad_low( uint64_t      i_nValues,
                    float const * i_a,
                    float const * i_b,
                    float       * o_c);
}

void triad_high(    uint64_t        i_nValues,
                    float    const * i_a,
                    float    const * i_b,
                    float          * o_c )
                {
    for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
        o_c[l_va] = i_a[l_va] + 2.0f * i_b[l_va];
    }
}


int main(int i_argc, char const * i_argv[]) {
    if (i_argc != 2) {
        std::cerr << "error" << std::endl;
        return EXIT_FAILURE;
    }

    uint64_t l_nEntries = strtoul(i_argv[1], NULL, 0);

    float * l_a = new float[l_nEntries];
    float * l_b = new float[l_nEntries];
    float * l_cRef = new float[l_nEntries];
    float * l_c = new float[l_nEntries];

    for (uint64_t l_va = 0; l_va < l_nEntries; l_va++) {
        l_a[l_va] = l_va % 100;
        l_b[l_va] = l_va % 100;
        l_cRef[l_va] = l_va % 100;
        l_c[l_va] = l_cRef[l_va];
    }

    triad_high(l_nEntries, l_a, l_b, l_cRef);
    triad_low(l_nEntries, l_a, l_b, l_c);

    for (uint64_t l_va = 0; l_va < l_nEntries; l_va++) {
        float l_diff = l_c[l_va] - l_cRef[l_va];
        l_diff = std::abs(l_diff);


        if (l_diff > 1E-6) {
            std::cerr << "position " << l_va << ": " << l_cRef[l_va] << " and " << l_c[l_va] << " don't match --> aborting" << std::endl;
            return EXIT_FAILURE;
        }
    }

    std::cout << "worked and all entries " << l_nEntries << " are matching" << std::endl;

    delete[] l_a;
    delete[] l_b;
    delete[] l_cRef;
    delete[] l_c;

    return EXIT_SUCCESS;
}