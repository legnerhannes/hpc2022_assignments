9.2) 2)
- Konsolen Ausgaben für driver.cpp mit emulierten 512 bit System:
    - Eingabe 4: worked and all entries 4 are matching -> 8 emulated instructions
    - Eingabe 8: worked and all entries 8 are matching -> 8 emulated instructions
    - Eingabe 15: worked and all entries 15 are matching -> 8 emulated instructions
    - Eingabe 16: worked and all entries 16 are matching -> 8 emulated instructions
    - Eingabe 17: worked and all entries 17 are matching -> 14 emulated instructions
    - Eingabe 24: worked and all entries 24 are matching -> 14 emulated instructions
    - Eingabe 32: worked and all entries 32 are matching -> 14 emulated instructions
    - Eingabe 33: worked and all entries 33 are matching -> 20 emulated instructions

-> Nach 16 Werten muss für ein 512 Bit System ein weiter Loop der Schleife im Assembler genutzt werden und da jeder Loop 6 Instruktionen enthält, erhöht sich die Zahl der emulierten Instruktionen immer um 6.
-> das liegt daran, dass mit single precision gerabeitet wird: 512 / 32 = 16