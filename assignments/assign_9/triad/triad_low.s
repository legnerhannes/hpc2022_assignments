        .text
        .type triad_low, %function
        .global triad_low
        /*
         * 
         */ 

triad_low:

        // init loop counter
        mov x4, #0

        // init predicate register (while less then)
        whilelt p0.s, x4, x0
        b.none end_loop

        // init all elements
        fmov z2.s, #2.0

loop:
        ld1w {z0.s}, p0/z, [x1, x4, lsl #2]
        ld1w {z1.s}, p0/z, [x2, x4, lsl #2]

        fmla z0.s, p0/m, z2.s, z1.s

        st1w z0.s, p0, [x3, x4, lsl #2]

        incw x4
        whilelt p0.s, x4, x0

        b.any loop

end_loop:

        ret
        .size triad_low, (. - triad_low)